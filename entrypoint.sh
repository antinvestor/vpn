#!/bin/sh
#
# Docker script to configure and start an IPsec VPN server
#

#Configure iptables for maxcom
iptables -t nat -A POSTROUTING -s 192.168.168.0/24 -d 10.0.0.0/8 -j MASQUERADE

# Start services
mkdir -p /var/run/pluto
rm -f /var/run/pluto/pluto.pid

modprobe af_key

# Start the actual IKE daemon
exec ipsec pluto --config /etc/ipsec.conf  --nofork --stderrlog

